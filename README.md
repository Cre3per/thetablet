# The Tablet

Manually track the game. Jump back to previous rounds. Mark players as
suspicious.  

Does not interact with the game in any way, you could do the same with pen and
paper.

![round 0](readmore/img/scaled/r0.png)

![help](readmore/img/scaled/help.png)

## Dependencies

- sdl2
- sdl2_gfx
- sdl2_image
- sdl2_ttf

## Download

It is recommended to build from sources. See [Building](#building)

### Linux

Download the release artifacts of the latest tag from
https://gitlab.com/Cre3per/thetablet/-/pipelines?page=1&scope=tags&status=success

## Usage

Press ':' to open the command line. Type "help" for a list of commands.

VIM-like shortcuts

### Recommended usage

In the emergency meeting, listen closely and update the map. You'll find
contradictions in what players say and detect groups.

## Development

### Building

```bash
cargo build
```
