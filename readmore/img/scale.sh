#!/bin/bash

set -Eeuxo pipefail

DIR="$(dirname $0)"

cd "${DIR}"

mkdir -p "scaled"

find -maxdepth 1 -name "*.png" -exec convert {} -scale 1024 "scaled/{}" \;
