#![allow(dead_code)]
#![allow(clippy::needless_return)]

extern crate sdl2;

mod the_tablet;

use the_tablet::{renderer::drawable::Drawable, serialize::Serializable, Tablet, TabletSerialize};

fn handle_event(tablet: &mut the_tablet::Tablet, event: sdl2::event::Event) -> bool {
    match event {
        sdl2::event::Event::Quit { .. } => {
            return false;
        }
        _ => {
            if !tablet.handle_event(&event) {
                // println!("unhandled event: {:?}", event);
            }
        }
    }

    true
}

fn handle_events(event_pump: &mut sdl2::EventPump, tablet: &mut the_tablet::Tablet) -> bool {
    for event in event_pump.poll_iter() {
        if !handle_event(tablet, event) {
            return false;
        }
    }

    true
}

fn render_scene(
    renderer: &mut the_tablet::renderer::renderer::Renderer,
    tablet: &mut the_tablet::Tablet,
) {
    tablet.draw(renderer);
}

fn render<'a>(
    renderer: &mut the_tablet::renderer::renderer::Renderer,
    tablet: &mut the_tablet::Tablet,
) {
    tablet.pre_render(renderer);

    renderer.canvas.set_draw_color(sdl2::pixels::Color::BLACK);
    renderer.canvas.clear();

    render_scene(renderer, tablet);

    renderer.canvas.present();
}

fn get_save_name() -> &'static str {
    return "save.yaml";
}

fn write_save(tablet: &Tablet) {
    let serialized = serde_yaml::to_string(&tablet.serialize()).unwrap();
    std::fs::write(get_save_name(), serialized).unwrap();
}

fn try_load_save() -> Tablet {
    match std::fs::read_to_string(get_save_name()) {
        Ok(serialized) => {
            if let Ok(serialized) = &serde_yaml::from_str::<TabletSerialize>(&serialized) {
              return Tablet::deserialize(serialized);
            } else {
              println!("Deserialization failed. Creating empty tablet.");
              return Tablet::new();
            }
        }
        _ => Tablet::new(),
    }
}

fn main() {
    let context = sdl2::init().unwrap();
    let video = context.video().unwrap();

    let window = video
        .window("The Tablet", 800, 450)
        .resizable()
        .maximized()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().present_vsync().build().unwrap();
    let texture_creator = canvas.texture_creator();
    let ttf_context = sdl2::ttf::init().unwrap();

    let mut renderer =
        the_tablet::renderer::renderer::Renderer::new(&mut canvas, &texture_creator, &ttf_context);

    let mut tablet = try_load_save();

    let mut event_pump = context.event_pump().unwrap();

    loop {
        if !handle_events(&mut event_pump, &mut tablet) {
            break;
        }
        render(&mut renderer, &mut tablet);
    }

    write_save(&tablet);

    println!("window closed");
}
