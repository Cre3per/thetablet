use std::rc::Rc;

use super::Tablet;

#[derive(Clone, Copy)]
pub enum CommandParameters {
    Any,
    Exactly(usize),
    Min(usize),
    Max(usize),
    MinMax(usize, usize),
}

pub type CommandCallback = dyn Fn(&mut Tablet, &Vec<&str>);
pub type CommandValidator = dyn Fn(&Tablet, &Vec<&str>) -> bool;

#[derive(Clone)]
pub struct Command {
    pub aliases: Vec<String>,
    pub help: String,
    pub parameters: CommandParameters,
    pub usage: String,
    pub callback: Rc<CommandCallback>,

    pub custom_validator: Option<Rc<CommandValidator>>,
}

impl Command {
    pub fn help(&mut self, help: &str) -> &mut Self {
        self.help = help.to_string();
        self
    }

    pub fn parameters(&mut self, parameters: CommandParameters) -> &mut Self {
        self.parameters = parameters;
        self
    }

    pub fn usage(&mut self, usage: &str) -> &mut Self {
        self.usage = format!("{} {}", self.aliases[0], usage.to_string());
        self
    }

    pub fn custom_validator(&mut self, validator: Rc<CommandValidator>) -> &mut Self {
        self.custom_validator = Some(validator.clone());
        self
    }

    pub fn get_name(&self) -> &String {
        &self.aliases[0]
    }

    pub fn new(aliases: &[&str], callback: Rc<CommandCallback>) -> Self {
        assert!(!aliases.is_empty());

        Self {
            aliases: aliases.iter().map(|alias| alias.to_string()).collect(),
            help: Default::default(),
            parameters: CommandParameters::Any,
            usage: format!("{} [arguments]", aliases[0]),
            callback,

            custom_validator: None,
        }
    }
}

impl Command {
    fn arguments_match_parameters(&self, args: &Vec<&str>) -> bool {
        match self.parameters {
            CommandParameters::Any => true,
            CommandParameters::Exactly(count) => args.len() == count,
            CommandParameters::Min(count) => args.len() >= count,
            CommandParameters::Max(count) => args.len() <= count,
            CommandParameters::MinMax(min, max) => (args.len() >= min) && (args.len() <= max),
        }
    }

    pub fn can_be_called_with_arguments(&self, tablet: &Tablet, args: &Vec<&str>) -> bool {
        if !self.arguments_match_parameters(args) {
            return false;
        }

        if self.custom_validator.is_some() {
            return self.custom_validator.as_ref().unwrap()(tablet, args);
        }

        return true;
    }

    pub fn is_named_by(&self, name: &str) -> bool {
        for alias in &self.aliases {
            if alias == name {
                return true;
            }
        }

        return false;
    }

    pub fn call(&self, tablet: &mut Tablet, args: &Vec<&str>) {
        (self.callback)(tablet, args);
    }
}
