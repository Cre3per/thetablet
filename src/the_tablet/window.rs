extern crate sdl2;

pub struct Window {
    // pub window: sdl2::video::Window,
    pub canvas: sdl2::render::Canvas<sdl2::video::Window>,
    pub texture_creator: sdl2::render::TextureCreator<sdl2::video::WindowContext>,
    pub ttf_context: sdl2::ttf::Sdl2TtfContext,
}

impl Window {
    pub fn new(
        sdl: &sdl2::Sdl,
        ttf_context: sdl2::ttf::Sdl2TtfContext,
        title: &str,
        width: usize,
        height: usize,
    ) -> Self {
        let video = sdl.video().unwrap();

        let window = video
            .window(title, width as u32, height as u32)
            .opengl()
            .build()
            .unwrap();

        let canvas = window.into_canvas().present_vsync().build().unwrap();
        let texture_creator = canvas.texture_creator();

        Self {
            // window,
            canvas,
            texture_creator,
            ttf_context,
        }
    }
}
