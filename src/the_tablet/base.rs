extern crate sdl2;

use super::{
    color::{Color, COLOR_COUNT},
    renderer::{drawable::Drawable, renderer::Renderer, shape},
    vector::Vector2f,
    Player,
};

#[derive(Default)]
pub struct Base {
    render_position: Vector2f,
    render_size: Vector2f,
}

impl Base {
    pub fn set_render_position(&mut self, position: &Vector2f) {
        self.render_position = *position;
    }

    pub fn get_render_position(&self) -> &Vector2f {
        &self.render_position
    }

    pub fn set_render_size(&mut self, size: &Vector2f) {
        self.render_size = *size;
    }

    pub fn get_color_slot(color: Color) -> usize {
        color as usize
    }

    pub fn get_player_slot(player: &Player) -> usize {
        Base::get_color_slot(player.color())
    }

    pub fn get_slot_count() -> usize {
        Color::Yellow as usize
    }

    pub fn get_players_per_row(&self) -> usize {
        return 2;
    }

    /**
     * Local pixel coordinates
     */
    pub fn get_slot_position(&self, slot: usize) -> Vector2f {
        let players_per_row = self.get_players_per_row();
        let players_per_column: usize = (COLOR_COUNT + players_per_row - 1) / players_per_row;

        let max_player_count: Vector2f =
            Vector2f::new(players_per_row as f32, players_per_column as f32);
        let max_player_count_1: Vector2f = max_player_count + Vector2f::new(1.0, 1.0);

        let min: Vector2f = Vector2f::new(1.0, 1.0) / max_player_count_1;
        let max: Vector2f = max_player_count / max_player_count_1;
        let size: Vector2f = max - min;

        let fractional = min
            + size
                * (Vector2f::new(
                    (slot % players_per_row) as f32 / ((players_per_row - 1).max(1) as f32),
                    ((slot / players_per_row) as f32) / ((players_per_column - 1).max(1) as f32),
                ));

        return self.render_size * fractional;
    }
}

impl Drawable for Base {
    fn draw(&self, renderer: &mut Renderer) {
        const BORDER_WIDTH: f32 = 1.0;

        shape::Rectangle::new(
            &self.render_position,
            &self.render_size,
            sdl2::pixels::Color::BLACK,
        )
        .filled()
        .outlined()
        .outline_color(sdl2::pixels::Color::WHITE)
        .draw(renderer);

        renderer.canvas.set_draw_color(sdl2::pixels::Color::WHITE);

        for i in 0..COLOR_COUNT {
            let slot_position = self.render_position + self.get_slot_position(i);

            renderer
                .canvas
                .draw_point(sdl2::rect::Point::new(
                    slot_position.x as i32,
                    slot_position.y as i32,
                ))
                .unwrap();
        }
    }
}
