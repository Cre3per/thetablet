use super::{
    command_manager::CommandManager,
    renderer::{drawable::Drawable, renderer::Renderer, shape},
    textbox::Textbox,
    vector::Vector2f,
};

struct Suggestion {
    full_path: Vec<String>,
    help: String,
}

#[derive(Default)]
pub struct ShowCommand {
    active_shortcut_path: Vec<String>,
    keystrokes: Textbox,
    suggestions: Vec<Suggestion>,
    target_size: Vector2f,
}

impl ShowCommand {
    pub fn fit_to_rectangle(&mut self, _position: &Vector2f, size: &Vector2f) {
        let keystrokes_size = Vector2f::new(64.0, 32.0);
        let padding = Vector2f::new(4.0, 4.0);

        self.keystrokes
            .set_render_position(&(*size - padding - keystrokes_size));
        self.keystrokes.set_render_size(&keystrokes_size);

        self.target_size = size.clone();
    }

    fn build_suggestions(&mut self, command_manager: &CommandManager) {
        self.suggestions.clear();

        if command_manager.get_active_shortcut_path().is_empty() {
            return;
        }

        let candidates = command_manager.get_shortcut_candidates();

        for candidate in &candidates {
            self.suggestions.push(Suggestion {
                full_path: candidate.path.clone(),
                help: candidate.value.help.clone(),
            });
        }
    }

    pub fn update(&mut self, command_manager: &CommandManager) {
        self.active_shortcut_path = command_manager.get_active_shortcut_path().clone();

        if self.active_shortcut_path.is_empty() {
            self.keystrokes.hide();
        } else {
            self.keystrokes.set_text(
                self.active_shortcut_path
                    .iter()
                    .fold(String::default(), |a, b| a + b),
            );
            self.keystrokes.show();
        }

        self.build_suggestions(command_manager);
    }

    pub fn new() -> Self {
        let mut s = Self::default();

        s.keystrokes.deactivate();

        return s;
    }
}

impl ShowCommand {
    fn draw_suggestion(
        &self,
        renderer: &mut Renderer,
        suggestion: &Suggestion,
        position: &Vector2f,
    ) -> Vector2f {
        let mut active_position = *position;

        let max_path_length = 64.0;
        let font = "amongus.24";

        let matched_path = self
            .active_shortcut_path
            .iter()
            .fold(String::default(), |a, b| a + b);

        let mut first = shape::Text::new(
            matched_path.as_str(),
            font,
            &active_position,
            sdl2::pixels::Color::GRAY,
        );

        active_position.x += first.size(renderer).x;

        first.outlined().draw(renderer);

        let unmatched_path = suggestion
            .full_path
            .clone()
            .split_off(self.active_shortcut_path.len())
            .iter()
            .fold(String::default(), |a, b| a + b);

        shape::Text::new(
            unmatched_path.as_str(),
            font,
            &active_position,
            sdl2::pixels::Color::WHITE,
        )
        .outlined()
        .draw(renderer);

        let help = shape::Text::new(
            suggestion.help.as_str(),
            font,
            &(*position + Vector2f::new(max_path_length, 0.0)),
            sdl2::pixels::Color::WHITE,
        );

        let help_size = help.size(renderer);

        help.draw(renderer);

        return Vector2f::new(max_path_length, 0.0) + help_size;
    }

    fn draw_suggestions_text(&self, renderer: &mut Renderer, position: &Vector2f) -> Vector2f {
        let line_height = 32.0;

        let mut i = 0;

        let mut size = Vector2f::new(0.0, (self.suggestions.len() as f32) * line_height);

        for suggestion in &self.suggestions {
            let suggestion_size = self.draw_suggestion(
                renderer,
                &suggestion,
                &(*position + Vector2f::new(0.0, i as f32 * line_height)),
            );

            size.x = size.x.max(suggestion_size.x);

            i += 1;
        }

        return size;
    }

    fn draw_suggestions(&self, renderer: &mut Renderer) {
        let off_screen = Vector2f::new(10_000.0, 10_000.0);
        let size = self.draw_suggestions_text(renderer, &off_screen);
        let padding_to_keystrokes = 4.0;

        let position = *self.keystrokes.get_render_position() - size
            + Vector2f::new(self.keystrokes.get_render_size().x, -padding_to_keystrokes);

        shape::Rectangle::new(&position, &size, sdl2::pixels::Color::BLACK)
            .filled()
            .outlined()
            .outline_color(sdl2::pixels::Color::WHITE)
            .draw(renderer);

        self.draw_suggestions_text(renderer, &position);
    }
}

impl Drawable for ShowCommand {
    fn draw(&self, renderer: &mut Renderer) {
        if self.active_shortcut_path.is_empty() {
            return;
        }

        self.keystrokes.draw(renderer);

        self.draw_suggestions(renderer);
    }
}
