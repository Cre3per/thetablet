extern crate sdl2;

use super::{
    renderer::{drawable::Drawable, renderer::Renderer, shape},
    vector::Vector2f,
};

use sdl2::rect::Rect;
use substring::Substring;

#[derive(Default)]
pub struct Textbox {
    render_position: Vector2f,
    render_size: Vector2f,

    text: String,

    visible: bool,
    active: bool,

    /**
     * Caret appears before the character at this index
     */
    caret_index: usize,
}

impl Textbox {
    pub fn get_render_position(&self) -> &Vector2f {
        &self.render_position
    }

    pub fn set_render_position(&mut self, render_position: &Vector2f) {
        self.render_position = *render_position;
    }

    pub fn get_render_size(&self) -> &Vector2f {
        &self.render_size
    }

    pub fn set_render_size(&mut self, render_size: &Vector2f) {
        self.render_size = *render_size;
    }

    pub fn set_text<T: AsRef<str>>(&mut self, text: T) {
        self.text = text.as_ref().to_string();
        self.caret_index = text.as_ref().len();
    }

    pub fn get_text(&self) -> &String {
        return &self.text;
    }

    pub fn show(&mut self) {
        self.visible = true;
    }

    pub fn hide(&mut self) {
        self.visible = false;
    }

    pub fn activate(&mut self) {
        self.active = true;
    }

    pub fn deactivate(&mut self) {
        self.active = false;
    }

    pub fn is_visible(&self) -> bool {
        return self.visible;
    }

    pub fn on_arrow(&mut self, left: bool) {
        if left {
            if self.caret_index > 0 {
                self.caret_index -= 1;
            }
        } else {
            if self.caret_index < self.text.len() {
                self.caret_index += 1;
            }
        }
    }

    pub fn on_character(&mut self, character: char) {
        self.text.insert(self.caret_index, character);
        self.caret_index += 1;
    }

    pub fn on_backspace(&mut self) {
        if self.caret_index > 0 {
            self.text.remove(self.caret_index - 1);
            self.caret_index -= 1;
        }
    }

    pub fn on_delete(&mut self) {
        if self.caret_index < self.text.len() {
            self.text.remove(self.caret_index);
        }
    }

    pub fn new(render_position: &Vector2f, render_size: &Vector2f) -> Self {
        Self {
            render_position: *render_position,
            render_size: *render_size,
            visible: true,
            active: true,
            text: String::default(),
            caret_index: 0,
        }
    }
}

impl Textbox {
    fn draw_caret(&self, renderer: &mut Renderer, font: &str) {
        let first_size = shape::Text::new(
            self.text.substring(0, self.caret_index),
            font,
            &Vector2f::default(),
            sdl2::pixels::Color::MAGENTA,
        )
        .size(renderer);

        let caret_position = first_size.x;

        renderer.canvas.set_draw_color(sdl2::pixels::Color::WHITE);
        renderer
            .canvas
            .draw_rect(Rect::new(
                (self.render_position.x + caret_position) as i32,
                self.render_position.y as i32,
                1,
                first_size.y as u32,
            ))
            .unwrap();
    }
}

impl Drawable for Textbox {
    fn draw(&self, renderer: &mut Renderer) {
        if !self.is_visible() {
            return;
        }

        shape::Rectangle::new(
            &self.render_position,
            &self.render_size,
            sdl2::pixels::Color::BLACK,
        )
        .filled()
        .outlined()
        .outline_color(sdl2::pixels::Color::WHITE)
        .draw(renderer);

        let font = "amongus.24";

        shape::Text::new(
            self.text.as_str(),
            font,
            &self.render_position,
            sdl2::pixels::Color::WHITE,
        )
        .draw(renderer);

        if self.active {
            self.draw_caret(renderer, font);
        }
    }
}
