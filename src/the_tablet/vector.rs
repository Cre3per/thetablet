extern crate sdl2;

use std::ops;

#[derive(Debug, Clone, Copy, serde::Serialize, serde::Deserialize)]
pub struct Vector2<T> {
    pub x: T,
    pub y: T,
}

impl<T: Copy> Vector2<T> {
    pub fn new(x: T, y: T) -> Self {
        Self { x, y }
    }
}

impl<T: Default> Default for Vector2<T> {
    fn default() -> Self {
        Self {
            x: T::default(),
            y: T::default(),
        }
    }
}

impl<T: ops::Add<T, Output = T> + Copy> ops::Add<Vector2<T>> for Vector2<T> {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl<T: ops::Sub<T, Output = T> + Copy> ops::Sub<Vector2<T>> for Vector2<T> {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

// Component-wise
impl<T: ops::Mul<T, Output = T> + Copy> ops::Mul<Vector2<T>> for Vector2<T> {
    type Output = Self;

    fn mul(self, other: Self) -> Self {
        Self {
            x: self.x * other.x,
            y: self.y * other.y,
        }
    }
}

// Scalar
impl<T: ops::Mul<T, Output = T> + Copy> ops::Mul<T> for Vector2<T> {
    type Output = Self;

    fn mul(self, other: T) -> Self {
        Self {
            x: self.x * other,
            y: self.y * other,
        }
    }
}

// Component-wise
impl<T: ops::Div<T, Output = T> + Copy> ops::Div<Vector2<T>> for Vector2<T> {
    type Output = Self;

    fn div(self, other: Self) -> Self {
        Self {
            x: self.x / other.x,
            y: self.y / other.y,
        }
    }
}

// Scalar
impl<T: ops::Div<T, Output = T> + Copy> ops::Div<T> for Vector2<T> {
    type Output = Self;

    fn div(self, other: T) -> Self {
        Self {
            x: self.x / other,
            y: self.y / other,
        }
    }
}

impl<T: std::cmp::PartialOrd> Vector2<T> {
    pub fn is_in_rectangle(&self, top_left: &Vector2<T>, bottom_right: &Vector2<T>) -> bool {
        return (self.x >= top_left.x)
            && (self.y >= top_left.y)
            && (self.x <= bottom_right.x)
            && (self.y <= bottom_right.y);
    }
}

pub type Vector2f = Vector2<f32>;
