pub trait Serializable<T> {
    fn serialize(&self) -> T;
    fn deserialize(serialized: &T) -> Self;
}
