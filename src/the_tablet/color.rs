use std::fmt::Display;

#[derive(Clone, Copy, PartialEq, Eq, Hash, serde::Serialize, serde::Deserialize)]
pub enum Color {
    Black,
    Blue,
    Brown,
    Cyan,
    Fortgreen,
    Green,
    Lime,
    Orange,
    Pink,
    Purple,
    Red,
    White,
    Yellow,
}
pub const COLOR_COUNT: usize = 13;

impl Color {
    pub fn from_name<T: AsRef<str>>(name: T) -> Option<Color> {
        match name.as_ref().to_lowercase().as_str() {
            "black" => Some(Color::Black),
            "blue" => Some(Color::Blue),
            "brown" => Some(Color::Brown),
            "cyan" => Some(Color::Cyan),
            "fortgreen" => Some(Color::Fortgreen),
            "green" => Some(Color::Green),
            "lime" => Some(Color::Lime),
            "orange" => Some(Color::Orange),
            "pink" => Some(Color::Pink),
            "purple" => Some(Color::Purple),
            "red" => Some(Color::Red),
            "white" => Some(Color::White),
            "yellow" => Some(Color::Yellow),
            _ => None,
        }
    }

    pub fn to_string(color: Color) -> &'static str {
        match color {
            Color::Black => "Black",
            Color::Blue => "Blue",
            Color::Brown => "Brown",
            Color::Cyan => "Cyan",
            Color::Fortgreen => "Fortgreen",
            Color::Green => "Green",
            Color::Lime => "Lime",
            Color::Orange => "Orange",
            Color::Pink => "Pink",
            Color::Purple => "Purple",
            Color::Red => "Red",
            Color::White => "White",
            Color::Yellow => "Yellow",
        }
    }

    pub fn to_rgb(&self) -> u32 {
        match self {
            Color::Black => 0x3f474e,
            Color::Blue => 0x132ed1,
            Color::Brown => 0x71491e,
            Color::Cyan => 0x38fedb,
            Color::Fortgreen => 0x1d9853,
            Color::Green => 0x117f2d,
            Color::Lime => 0x50ef39,
            Color::Orange => 0xef7d0d,
            Color::Pink => 0xed54ba,
            Color::Purple => 0x6b2fbb,
            Color::Red => 0xc51111,
            Color::White => 0xd6e0f0,
            Color::Yellow => 0xf6f658,
        }
    }

    pub fn to_sdl2(&self) -> sdl2::pixels::Color {
      let rgb = self.to_rgb();
      return sdl2::pixels::Color::RGB(((rgb >> 16) & 0xff) as u8, ((rgb >> 8) & 0xff) as u8, (rgb & 0xff) as u8);
    }
}

impl Display for Color {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", Color::to_string(*self))
    }
}
