extern crate sdl2;

use std::path::Path;
use std::{collections::HashMap, path::PathBuf};

use sdl2::render::*;
use sdl2::{image::*, video::WindowContext};
use std::rc::*;

use super::resource::{Locator, TEXTURE_RESOURCE};

pub struct TextureStore<'s, Key> {
    texture_creator: &'s TextureCreator<WindowContext>,
    sources: HashMap<Key, PathBuf>,
    textures: HashMap<Key, Rc<Texture<'s>>>,
}

// TODO: Remove redundant constraint
impl<'s, Key: std::cmp::Eq + std::hash::Hash + Clone> TextureStore<'s, Key> {
    pub fn has(&self, key: &Key) -> bool {
        return self.textures.contains_key(key);
    }

    fn load<T: AsRef<Path>>(&self, path: T) -> Rc<Texture<'s>> {
        let str_path = path.as_ref().to_str().unwrap();
        let maybe_texture = self.texture_creator.load_texture(&path);

        match maybe_texture {
            Ok(texture) => {
                return Rc::new(texture);
                // self.textures.insert(str_path.to_string(), Rc::new(texture));
                // return self.textures.get(&str_path.to_string()).unwrap().clone();
            }
            Err(str) => panic!("no such texture {}: {}", str_path, str),
        }
    }

    pub fn get_mut(&mut self, key: &Key) -> &mut Texture<'s> {
        match self.textures.get_mut(key) {
            Some(boxed_texture) => {
                let mutable_texture: &mut Texture = Rc::get_mut(boxed_texture).unwrap();

                return mutable_texture;
            }
            None => {
                panic!("tried to use a texture that doesn't exist");
            }
        }
    }

    pub fn get(&self, key: &Key) -> Rc<Texture<'s>> {
        match self.textures.get(key) {
            Some(boxed_texture) => {
                return boxed_texture.clone();
            }
            None => {
                panic!("tried to use a texture that doesn't exist");
            }
        }
    }

    pub fn register<T: AsRef<Path>>(&mut self, key: &Key, path: T) {
        if !self.textures.contains_key(&key) {
            let full_path = TEXTURE_RESOURCE.need(path.as_ref());
            self.textures.insert(key.clone(), self.load(&full_path));
            self.sources
                .insert(key.clone(), path.as_ref().to_path_buf());
        }
    }

    pub fn clone(&mut self, from: &Key, to: &Key) -> Rc<Texture<'s>> {
        let source = self.sources.get(from).unwrap().clone();
        self.register(to, source);

        return self.get(to);
    }

    pub fn new(texture_creator: &'s TextureCreator<WindowContext>) -> Self {
        Self {
            texture_creator,
            sources: HashMap::default(),
            textures: HashMap::default(),
        }
    }
}
