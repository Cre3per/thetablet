use std::option::*;
use std::path::*;

pub trait Locator {
    fn localize(&self, path: &Path) -> PathBuf {
        path.to_path_buf()
    }

    fn find(&self, path: &Path) -> Option<PathBuf> {
        let search_directories = vec!["./res/", "/opt/thetablet/res/"];

        for directory in &search_directories {
            let candidate = Path::new(directory).join(self.localize(path));
            if candidate.exists() {
                return Some(candidate);
            }
        }

        return None;
    }

    fn need(&self, path: &Path) -> PathBuf {
        match self.find(path) {
            Some(p) => return p,
            _ => {
                panic!(
                    "no such resource: {}",
                    match self.localize(path).to_str() {
                        Some(s) => s,
                        _ => "None",
                    }
                );
            }
        }
    }
}

pub struct TextureResource {}

impl Locator for TextureResource {
    fn localize(&self, path: &Path) -> PathBuf {
        return Path::new("textures/").join(path).with_extension("png");
    }
}

pub static TEXTURE_RESOURCE: TextureResource = TextureResource {};

pub struct FontResource {}

impl Locator for FontResource {
    fn localize(&self, path: &Path) -> PathBuf {
        return Path::new("fonts/").join(path).with_extension("ttf");
    }
}

pub static FONT_RESOURCE: FontResource = FontResource {};

pub struct LevelResource {}

impl Locator for LevelResource {
    fn localize(&self, path: &Path) -> PathBuf {
        return Path::new("level/").join(path).with_extension("yaml");
    }
}

pub static LEVEL_RESOURCE: LevelResource = LevelResource {};
