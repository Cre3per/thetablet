pub struct AnimationTimer {
    time: f32,
}

impl AnimationTimer {
    pub fn time(&self) -> f32 {
        self.time
    }

    pub fn scale(&mut self, scale: f32) -> &mut Self {
        self.time *= scale;
        self
    }

    pub fn sin(&self) -> f32 {
        ((self.time() * (2.0 * std::f32::consts::PI)).sin() + 1.0) / 2.0
    }

    pub fn new(time: f32) -> Self {
        Self { time }
    }
}
