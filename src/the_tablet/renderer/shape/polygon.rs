use sdl2::gfx::primitives::DrawRenderer;

use crate::the_tablet::{
    renderer::{drawable::Drawable, renderer::Renderer},
    vector::Vector2f,
};

use super::shape::Shape;

pub struct Polygon {
    shape: Shape,

    vertexes: Vec<Vector2f>,

    filled: bool,
    outlined: bool,
    outline_width: f32,
    outline_color: sdl2::pixels::Color,
}

impl Polygon {
    pub fn filled(&mut self) -> &mut Self {
        self.filled = true;
        self
    }

    pub fn outlined(&mut self) -> &mut Self {
        self.outlined = true;
        self
    }

    pub fn outline_width(&mut self, width: f32) -> &mut Self {
        self.outline_width = width;
        self
    }

    pub fn outline_color(&mut self, color: sdl2::pixels::Color) -> &mut Self {
        self.outline_color = color;
        self
    }

    pub fn new(vertexes: &Vec<Vector2f>, color: sdl2::pixels::Color) -> Self {
        Self {
            shape: Shape { color },

            vertexes: vertexes.clone(),

            filled: false,
            outlined: false,
            outline_width: 1.0,
            outline_color: sdl2::pixels::Color::BLACK,
        }
    }
}

impl Polygon {
    fn draw_outline(&self, renderer: &mut Renderer) {
        if !self.filled {
            // I don't want to draw the outline _around_ the polygon, I'm drawing it
            // on the edge (That's different from what I do for rectangles).
            // Drawing the outline on the edge of a non-filles polygon would
            // override it.
            return;
        }

        Polygon::new(&self.vertexes, self.outline_color).draw(renderer);
    }
}

impl Drawable for Polygon {
    fn draw(&self, renderer: &mut Renderer) {
        let x: Vec<i16> = self.vertexes.iter().map(|v| v.x as i16).collect();
        let y: Vec<i16> = self.vertexes.iter().map(|v| v.y as i16).collect();

        if self.filled {
            renderer
                .canvas
                .filled_polygon(x.as_slice(), y.as_slice(), self.shape.color)
                .unwrap();
        } else {
            renderer
                .canvas
                .polygon(x.as_slice(), y.as_slice(), self.shape.color)
                .unwrap();
        }

        if self.outlined {
            self.draw_outline(renderer);
        }
    }
}
