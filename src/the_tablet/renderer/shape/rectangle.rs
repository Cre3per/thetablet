use crate::the_tablet::{
    renderer::{drawable::Drawable, renderer::Renderer},
    vector::Vector2f,
};

use super::shape::Shape;

pub struct Rectangle {
    shape: Shape,

    position: Vector2f,
    size: Vector2f,

    filled: bool,
    outlined: bool,
    outline_width: f32,
    outline_color: sdl2::pixels::Color,
}

impl Rectangle {
    pub fn filled(&mut self) -> &mut Self {
        self.filled = true;
        self
    }

    pub fn outlined(&mut self) -> &mut Self {
        self.outlined = true;
        self
    }

    pub fn outline_width(&mut self, width: f32) -> &mut Self {
        self.outline_width = width;
        self
    }

    pub fn outline_color(&mut self, color: sdl2::pixels::Color) -> &mut Self {
        self.outline_color = color;
        self
    }

    pub fn new(position: &Vector2f, size: &Vector2f, color: sdl2::pixels::Color) -> Self {
        Self {
            shape: Shape { color },

            position: *position,
            size: *size,

            filled: false,
            outlined: false,
            outline_width: 1.0,
            outline_color: sdl2::pixels::Color::BLACK,
        }
    }
}

impl Rectangle {
    fn draw_outline(&self, renderer: &mut Renderer) {
        let outline = Vector2f::new(self.outline_width, self.outline_width);

        if self.filled {
            Rectangle::new(
                &(self.position - outline),
                &(self.size + outline * 2.0),
                self.outline_color,
            )
            .filled()
            .draw(renderer);
        } else {
            // Doesn't support outline width
            Rectangle::new(
                &(self.position - outline),
                &(self.size + outline * 2.0),
                self.outline_color,
            )
            .draw(renderer);

            Rectangle::new(
                &(self.position + outline),
                &(self.size - outline * 2.0),
                self.outline_color,
            )
            .draw(renderer);
        }
    }
}

impl Drawable for Rectangle {
    fn draw(&self, renderer: &mut Renderer) {
        if self.outlined {
            self.draw_outline(renderer);
        }

        let rect = sdl2::rect::Rect::new(
            self.position.x as i32,
            self.position.y as i32,
            self.size.x as u32,
            self.size.y as u32,
        );

        renderer.canvas.set_draw_color(self.shape.color);

        if self.filled {
            renderer.canvas.fill_rect(rect).unwrap();
        } else {
            renderer.canvas.draw_rect(rect).unwrap();
        }
    }
}
