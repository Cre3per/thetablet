use crate::the_tablet::{
    renderer::{drawable::Drawable, renderer::Renderer},
    vector::Vector2f,
};

use super::shape::Shape;

pub struct Text {
    shape: Shape,

    text: String,
    font_key: String,

    position: Vector2f,
    centered_x: bool,
    centered_y: bool,

    filled: bool,

    outlined: bool,
    outline_width: f32,
    outline_color: sdl2::pixels::Color,

    shadow: bool,
    shadow_offset: Vector2f,
    shadow_color: sdl2::pixels::Color,
}

impl Text {
    pub fn center(&mut self, x: bool, y: bool) -> &mut Self {
        self.centered_x = x;
        self.centered_y = y;
        self
    }

    pub fn filled(&mut self) -> &mut Self {
        self.filled = true;
        self
    }

    pub fn outlined(&mut self) -> &mut Self {
        self.outlined = true;
        self
    }

    pub fn outline_width(&mut self, width: f32) -> &mut Self {
        self.outline_width = width;
        self
    }

    pub fn outline_color(&mut self, color: sdl2::pixels::Color) -> &mut Self {
        self.outline_color = color;
        self
    }

    pub fn shadow(&mut self) -> &mut Self {
        self.shadow = true;
        self
    }

    pub fn shadow_offset(&mut self, offset: &Vector2f) -> &mut Self {
        self.shadow_offset = *offset;
        self
    }

    pub fn shadow_color(&mut self, color: sdl2::pixels::Color) -> &mut Self {
        self.shadow_color = color;
        self
    }

    pub fn new(
        text: &str,
        font_key: &str,
        position: &Vector2f,
        color: sdl2::pixels::Color,
    ) -> Self {
        Self {
            shape: Shape { color },

            text: text.to_string(),
            font_key: font_key.to_string(),

            position: *position,
            centered_x: false,
            centered_y: false,

            filled: false,

            outlined: false,
            outline_width: 1.0,
            outline_color: sdl2::pixels::Color::BLACK,

            shadow: false,
            shadow_offset: Vector2f::new(1.0, 1.0),
            shadow_color: sdl2::pixels::Color::BLACK,
        }
    }
}

impl Text {
    fn make_texture<'r>(&self, renderer: &mut Renderer<'r>) -> sdl2::render::Texture<'r> {
        let font = renderer.font_store.get(&self.font_key);

        let surface = font
            .render(self.text.as_ref())
            .blended(self.shape.color)
            .unwrap();

        let texture = renderer
            .texture_creator
            .create_texture_from_surface(&surface)
            .unwrap();

        return texture;
    }
}

impl Text {
    pub fn size(&self, renderer: &mut Renderer) -> Vector2f {
        if self.text.is_empty() {
            return Vector2f::new(
                0.0,
                Text::new("X", &self.font_key, &self.position, self.shape.color)
                    .size(renderer)
                    .y,
            );
        }

        let texture = self.make_texture(renderer);

        let sdl2::render::TextureQuery { width, height, .. } = texture.query();

        return Vector2f::new(width as f32, height as f32);
    }
}

impl Text {
    fn draw_outline(&self, renderer: &mut Renderer) {
        let directions = [
            Vector2f::new(1.0, 0.0),
            Vector2f::new(0.0, 1.0),
            Vector2f::new(-1.0, 0.0),
            Vector2f::new(0.0, -1.0),
        ];

        for direction in &directions {
            for i in 1..(self.outline_width as i32 + 1) {
                let mut text = Text::new(
                    self.text.as_str(),
                    self.font_key.as_str(),
                    &(self.position + (*direction * (i as f32))),
                    self.outline_color,
                );

                text.center(self.centered_x, self.centered_y);

                text.draw(renderer);
            }
        }
    }

    fn draw_shadow(&self, renderer: &mut Renderer) {
        let mut text = Text::new(
            self.text.as_str(),
            self.font_key.as_str(),
            &(self.position + self.shadow_offset),
            self.shadow_color,
        );

        text.center(self.centered_x, self.centered_y);

        text.draw(renderer);
    }
}

impl Drawable for Text {
    fn draw(&self, renderer: &mut Renderer) {
        if self.text.is_empty() {
            return;
        }

        if self.outlined {
            self.draw_outline(renderer);
        }

        if self.shadow {
            self.draw_shadow(renderer);
        }

        renderer.canvas.set_draw_color(self.shape.color);

        let texture = self.make_texture(renderer);

        let sdl2::render::TextureQuery { width, height, .. } = texture.query();

        let mut target_rect = sdl2::rect::Rect::new(
            self.position.x as i32,
            self.position.y as i32,
            width,
            height,
        );

        if self.centered_x {
            target_rect.set_x(target_rect.x() - (target_rect.width() as i32) / 2);
        }

        if self.centered_y {
            target_rect.set_y(target_rect.y() - (target_rect.height() as i32) / 2);
        }

        renderer.canvas.copy(&texture, None, target_rect).unwrap();
    }
}
