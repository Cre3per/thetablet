extern crate sdl2;

pub struct Shape {
    pub color: sdl2::pixels::Color,
}

impl Default for Shape {
    fn default() -> Self {
        Self {
            color: sdl2::pixels::Color::MAGENTA,
        }
    }
}
