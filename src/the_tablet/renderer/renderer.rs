extern crate sdl2;

use sdl2::{
    render::{Canvas, TextureCreator},
    ttf::Sdl2TtfContext,
    video::{Window, WindowContext},
};

use super::{animation::AnimationTimer, font_store::FontStore, texture_store::TextureStore};

pub struct Renderer<'s> {
    creation: std::time::Instant,

    pub canvas: &'s mut Canvas<Window>,

    pub texture_creator: &'s TextureCreator<WindowContext>,
    pub texture_store: TextureStore<'s, String>,

    pub ttf_context: &'s Sdl2TtfContext,
    pub font_store: FontStore<'s, String>,
}

impl<'s> Renderer<'s> {
    fn age(&self) -> f32 {
        std::time::Instant::now()
            .duration_since(self.creation)
            .as_secs_f32()
    }

    pub fn animation(&self) -> AnimationTimer {
        AnimationTimer::new(self.age())
    }

    pub fn new(
        canvas: &'s mut Canvas<Window>,
        texture_creator: &'s TextureCreator<WindowContext>,
        ttf_context: &'s Sdl2TtfContext,
    ) -> Self {
        Self {
            creation: std::time::Instant::now(),

            canvas,

            texture_creator,
            texture_store: TextureStore::new(&texture_creator),

            ttf_context,
            font_store: FontStore::new(&ttf_context),
        }
    }
}
