extern crate sdl2;

use std::path::Path;
use std::{collections::HashMap, path::PathBuf};

use sdl2::ttf::*;
use std::rc::*;

use super::resource::{Locator, FONT_RESOURCE};

pub struct FontStore<'s, Key> {
    ttf_context: &'s Sdl2TtfContext,
    sources: HashMap<Key, PathBuf>,
    fonts: HashMap<Key, Rc<Font<'s, 's>>>,
}

impl<'s, Key: std::cmp::Eq + std::hash::Hash + Clone> FontStore<'s, Key> {
    pub fn has(&self, key: &Key) -> bool {
        return self.fonts.contains_key(key);
    }

    fn load<T: AsRef<Path>>(&self, path: T, point_size: usize) -> Rc<Font<'s, 's>> {
        let maybe_font = self.ttf_context.load_font(&path, point_size as u16);

        match maybe_font {
            Ok(font) => {
                return Rc::new(font);
            }
            Err(str) => panic!("no such font {}: {}", path.as_ref().to_str().unwrap(), str),
        }
    }

    pub fn get_mut(&mut self, key: &Key) -> &mut Font<'s, 's> {
        match self.fonts.get_mut(key) {
            Some(rc) => {
                return Rc::get_mut(rc).unwrap();
            }
            None => {
                panic!("tried to use a font that doesn't exist");
            }
        }
    }

    pub fn get(&mut self, key: &Key) -> Rc<Font<'s, 's>> {
        match self.fonts.get(key) {
            Some(rc) => {
                return rc.clone();
            }
            None => {
                panic!("tried to use a texture that doesn't exist");
            }
        }
    }

    pub fn register<T: AsRef<Path>>(&mut self, key: &Key, path: T, point_size: usize) {
        if !self.fonts.contains_key(&key) {
            let full_path = FONT_RESOURCE.need(path.as_ref());
            self.fonts
                .insert(key.clone(), self.load(&full_path, point_size));
            self.sources
                .insert(key.clone(), path.as_ref().to_path_buf());
        }
    }

    pub fn new(ttf_context: &'s Sdl2TtfContext) -> Self {
        Self {
            ttf_context,
            sources: HashMap::default(),
            fonts: HashMap::default(),
        }
    }
}
