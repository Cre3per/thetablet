use super::vector::*;
use super::{
    renderer::{
        drawable::Drawable,
        renderer::Renderer,
        resource::{Locator, LEVEL_RESOURCE},
    },
    serialize::Serializable,
    sprite::Sprite,
};
use serde::Deserialize;

use std::path::Path;

#[derive(Debug, Deserialize)]
struct LevelProperties {
    display_name: String,
    origin: Vector2f,
    size: Vector2f,
}

#[derive(Debug, Deserialize)]
struct LevelFile {
    level: LevelProperties,
}

// Red[Blue[Green()]]
// Red: Screen
// Blue: Playable
// Green: Player
pub struct Level {
    name: String,
    display_name: String,

    /**
     * In pixels relative to top left.
     */
    origin: Vector2f,

    /**
     * In world units.
     */
    size: Vector2f,

    render_position: Vector2f,
    render_size: Vector2f,
}

impl Level {
    pub fn get_sprite_key(&self) -> String {
        return self.name.clone();
    }

    pub fn get_sprite<'a>(&self, renderer: &mut Renderer<'a>) -> Sprite<'a> {
        renderer.texture_store.register(
            &self.get_sprite_key(),
            &format!("level/{}", self.get_sprite_key()),
        );

        Sprite::from_resource(renderer, &self.get_sprite_key())
    }

    pub fn screen_to_world(&self, position: &Vector2f) -> Vector2f {
        let relative_to_level = *position - self.render_position;
        let fractional = relative_to_level / self.render_size;

        return fractional * self.size;
    }

    pub fn world_to_screen(&self, position: &Vector2f) -> Vector2f {
        let fractional = *position / self.size;
        let relative_to_level = fractional * self.render_size;

        let result = self.render_position + relative_to_level;

        return result;
    }

    fn set_render_position(&mut self, position: &Vector2f) {
        self.render_position = *position;
    }

    pub fn get_render_position(&self) -> &Vector2f {
        return &self.render_position;
    }

    pub fn get_render_size(&self) -> &Vector2f {
        return &self.render_size;
    }

    pub fn fit_to_rectangle(&mut self, renderer: &mut Renderer, size: &Vector2f) {
        let sprite = self.get_sprite(renderer);
        let texture_rect = *sprite.get_original_size();
        let ratios = texture_rect / *size;

        let max_ratio = if ratios.x > ratios.y {
            ratios.x
        } else {
            ratios.y
        };

        self.render_size = texture_rect / max_ratio;
    }

    pub fn origin(&self) -> &Vector2f {
        return &self.origin;
    }

    pub fn get_size(&self) -> &Vector2f {
        return &self.size;
    }

    pub fn new(name: &str) -> Self {
        let yaml_string = std::fs::read_to_string(LEVEL_RESOURCE.need(Path::new(&name))).unwrap();
        let settings = serde_yaml::from_str::<LevelFile>(yaml_string.as_str())
            .unwrap()
            .level;

        Self {
            name: name.to_string(),
            display_name: settings.display_name,

            origin: settings.origin,
            size: settings.size,

            render_position: Default::default(),
            render_size: Default::default(),
        }
    }

    pub fn from_name(name: &str) -> Option<Level> {
        if LEVEL_RESOURCE.find(Path::new(&name)).is_some() {
            return Some(Level::new(name));
        } else {
            return None;
        }
    }

    pub fn default() -> Self {
        Level::new("skeld")
    }
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct LevelSerialize {
    name: String,
}

impl Serializable<LevelSerialize> for Level {
    fn serialize(&self) -> LevelSerialize {
        LevelSerialize {
            name: self.name.clone(),
        }
    }

    fn deserialize(serialized: &LevelSerialize) -> Self {
        Level::new(serialized.name.as_str())
    }
}

impl Drawable for Level {
    fn draw(&self, renderer: &mut Renderer) {
        let mut sprite = self.get_sprite(renderer);

        sprite.set_size(&self.render_size);

        sprite.draw(renderer);
    }
}
