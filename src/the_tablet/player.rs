use std::convert::TryFrom;

use super::{
    color::Color,
    renderer::{drawable::Drawable, renderer::Renderer, shape},
    serialize::Serializable,
    vector::*,
};
use super::{hitbox::HitRectangle, sprite::Sprite, sprite_group::SpriteGroup};

use sdl2::render::Texture;

#[derive(Clone, Copy, serde::Serialize, serde::Deserialize)]
pub enum Trust {
    Trusted,
    Questionable,
    Suspicious,
}

impl Trust {
    pub fn get_sdl2_color(&self) -> sdl2::pixels::Color {
        match self {
            Trust::Trusted => sdl2::pixels::Color::GREEN,
            Trust::Questionable => sdl2::pixels::Color::YELLOW,
            Trust::Suspicious => sdl2::pixels::Color::RED,
        }
    }
}

impl TryFrom<&str> for Trust {
    type Error = ();

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value {
            "trusted" => Ok(Trust::Trusted),
            "questionable" => Ok(Trust::Questionable),
            "suspicious" => Ok(Trust::Suspicious),
            _ => Err(()),
        }
    }
}

pub struct Player {
    index: usize,

    color: Color,
    name: String,

    alive: bool,

    trust: Option<Trust>,

    /**
     * In game-units. May be used for path-finding or task
     * checking.
     */
    position: Vector2f,

    /**
     * In game-units.
     */
    visual_size: Vector2f,

    render_position: Vector2f,
    render_width: f32,
}

#[derive(Clone, serde::Serialize, serde::Deserialize)]
pub struct PlayerSerialize {
    color: Color,
    name: String,
    alive: bool,
    trust: Option<Trust>,
    position: Vector2f,
}

impl Player {
    pub fn index(&self) -> usize {
        return self.index;
    }

    pub fn set_index(&mut self, index: usize) {
        self.index = index;
    }

    pub fn color(&self) -> Color {
        return self.color;
    }

    pub fn set_color(&mut self, color: Color) {
        self.color = color;
    }

    pub fn name(&self) -> &String {
        return &self.name;
    }

    pub fn set_name(&mut self, name: &str) {
        self.name = name.to_string();
    }

    pub fn position(&self) -> &Vector2f {
        return &self.position;
    }

    pub fn visual_size(&self) -> &Vector2f {
        return &self.visual_size;
    }

    /**
     * World hitbox
     */
    pub fn hitbox(&self) -> HitRectangle {
        let half_visual_size = self.visual_size / 2.0;

        HitRectangle {
            top_left: self.position - half_visual_size,
            bottom_right: self.position + half_visual_size,
        }
    }

    pub fn set_position(&mut self, position: &Vector2f) {
        self.position = *position;
    }

    pub fn set_alive(&mut self, alive: bool) {
        self.alive = alive;
    }

    pub fn get_trust(&self) -> Option<Trust> {
        self.trust
    }

    pub fn set_trust(&mut self, trust: Option<Trust>) {
        self.trust = trust;
    }

    /**
     * @return Absolute screen pixels, top left of the player.
     */
    pub fn get_render_position(&self) -> &Vector2f {
        &self.render_position
    }

    /**
     * @param render_position Absolute screen pixels, top left of the player.
     */
    pub fn set_render_position(&mut self, render_position: Vector2f) {
        self.render_position = render_position;
    }

    /**
     * Height is calculated based on the original aspect ratio.
     */
    pub fn set_render_size(&mut self, width: f32) {
        self.render_width = width;
    }

    pub fn get_render_size(&self) -> Vector2f {
        // TODO: This is the size of the texture in pixels
        const ASPECT_RATIO: f32 = 587.0 / 744.0;

        return Vector2f::new(self.render_width, self.render_width / ASPECT_RATIO);
    }

    pub fn new(index: usize, color: Color, name: String) -> Self {
        Self {
            index,

            color,
            name,

            alive: true,

            trust: None,

            position: Vector2f::default(),
            visual_size: Vector2f::new(50.0, 64.0),

            render_position: Vector2f::default(),
            render_width: 0.0,
        }
    }

    pub fn default(index: usize) -> Self {
        Player::new(index, Color::Yellow, "Dummy".to_string())
    }
}

// Drawable
impl Player {
    fn get_color_texture_key(color: Color) -> String {
        format!("crewmate.color.{}", color as i32)
    }

    fn get_dead_color_texture_key(color: Color) -> String {
        format!("crewmate.dead.color.{}", color as i32)
    }

    fn make_colored_texture(
        renderer: &mut Renderer,
        source_key: &str,
        destination_key: &str,
        color: Color,
    ) {
        if !renderer.texture_store.has(&destination_key.to_string()) {
            renderer
                .texture_store
                .clone(&source_key.to_string(), &destination_key.to_string());

            let texture: &mut Texture =
                renderer.texture_store.get_mut(&destination_key.to_string());

            let color = color.to_rgb();

            texture.set_color_mod(
                ((color & (0xff << 16)) >> 16) as u8,
                ((color & (0xff << 8)) >> 8) as u8,
                (color & 0xff) as u8,
            );
        }
    }

    fn register_colored_textures(renderer: &mut Renderer, color: Color) {
        Player::make_colored_texture(
            renderer,
            "crewmate.color",
            Player::get_color_texture_key(color).as_str(),
            color,
        );
        Player::make_colored_texture(
            renderer,
            "crewmate.dead.color",
            Player::get_dead_color_texture_key(color).as_str(),
            color,
        );
    }

    fn register_common_textures(renderer: &mut Renderer) {
        renderer
            .texture_store
            .register(&"crewmate.static".to_string(), "crewmate/static");
        renderer
            .texture_store
            .register(&"crewmate.color".to_string(), "crewmate/color");

        renderer
            .texture_store
            .register(&"crewmate.dead.static".to_string(), "crewmate/static_dead");
        renderer
            .texture_store
            .register(&"crewmate.dead.color".to_string(), "crewmate/color_dead");
    }

    fn register_textures(renderer: &mut Renderer, color: Color) {
        Player::register_common_textures(renderer);
        Player::register_colored_textures(renderer, color);
    }

    fn make_sprites<'a>(renderer: &mut Renderer<'a>, color: Color) -> SpriteGroup<'a> {
        Player::register_textures(renderer, color);

        let crewmate_body =
            Sprite::from_resource(renderer, Player::get_color_texture_key(color).as_str());
        let crewmate_static = Sprite::from_resource(renderer, &"crewmate.static".to_string());

        return SpriteGroup::new(vec![crewmate_static, crewmate_body]);
    }

    fn make_dead_sprites<'a>(renderer: &mut Renderer<'a>, color: Color) -> SpriteGroup<'a> {
        Player::register_textures(renderer, color);

        let crewmate_body =
            Sprite::from_resource(renderer, Player::get_dead_color_texture_key(color).as_str());
        let crewmate_static = Sprite::from_resource(renderer, &"crewmate.dead.static".to_string());

        return SpriteGroup::new(vec![crewmate_static, crewmate_body]);
    }

    fn get_name_tag_anchor(&self) -> Vector2f {
        Vector2f::new(
            self.render_position.x + self.render_width / 2.0,
            self.render_position.y - 24.0,
        )
    }

    fn draw_trust(&self, renderer: &mut Renderer) {
        let position = self.get_name_tag_anchor();

        if let Some(trust) = self.trust {
            const LENGTH:f32 = 10.0;

            let square_size = match trust {
              Trust::Trusted => Vector2f::new(LENGTH / 2.0, LENGTH),
              Trust::Questionable => Vector2f::new(LENGTH, LENGTH / 2.0),
              Trust::Suspicious => Vector2f::new(LENGTH, LENGTH),
            };

            let square_position =
                Vector2f::new(position.x - square_size.x / 2.0, position.y - square_size.y);

            shape::Rectangle::new(&square_position, &square_size, trust.get_sdl2_color())
                .filled()
                .outlined()
                .draw(renderer);
        }
    }

    fn draw_name_tag(&self, renderer: &mut Renderer) {
        let name_anchor = self.get_name_tag_anchor();

        shape::Text::new(
            self.name().as_str(),
            "amongus.24",
            &name_anchor,
            sdl2::pixels::Color::WHITE,
        )
        .outlined()
        .outline_color(sdl2::pixels::Color::BLACK)
        .center(true, false)
        .draw(renderer);
    }

    fn draw_body(&self, renderer: &mut Renderer) {
        let mut sprites = if self.alive {
            Player::make_sprites(renderer, self.color)
        } else {
            Player::make_dead_sprites(renderer, self.color)
        };

        sprites.set_position(&self.render_position);

        let original_texture_size = sprites.get_sprite(0).get_original_size();
        let aspect_ratio = original_texture_size.x / original_texture_size.y;

        sprites.set_size(&Vector2f::new(
            self.render_width,
            self.render_width / aspect_ratio,
        ));

        sprites.draw(renderer);
    }
}

impl Serializable<PlayerSerialize> for Player {
    fn serialize(&self) -> PlayerSerialize {
        PlayerSerialize {
            color: self.color,
            name: self.name.clone(),
            alive: self.alive,
            trust: self.trust,
            position: self.position,
        }
    }

    fn deserialize(serialized: &PlayerSerialize) -> Self {
        let mut r = Player::new(usize::MAX, serialized.color, serialized.name.clone());

        r.alive = serialized.alive;
        r.trust = serialized.trust;
        r.position = serialized.position;

        return r;
    }
}

impl Drawable for Player {
    fn draw(&self, renderer: &mut Renderer) {
        self.draw_name_tag(renderer);
        self.draw_trust(renderer);
        self.draw_body(renderer);
    }
}
