use std::{cell::RefCell, collections::BTreeMap, convert::TryFrom, rc::Rc};

use sdl2::{event::Event, keyboard::Keycode};
use showcommand::ShowCommand;
use textbox::Textbox;

use super::{
    base::Base,
    command::{Command, CommandParameters},
    popup::Popup,
    renderer::shape,
    showcommand,
};
use super::{color::Color, command_manager::CommandManager};
use super::{hitbox::*, textbox};
use super::{
    renderer::{drawable::Drawable, renderer::Renderer},
    Player,
};
use super::{serialize::Serializable, vector::Vector2f, LevelSerialize, PlayerSerialize};
use super::{Level, Trust};

struct DragTarget {
    pub player_index: usize,
    pub offset: Vector2f,
}

#[derive(Default, Clone)]
struct Round {
    players: Vec<PlayerSerialize>,
    based_colors: Vec<Color>,
}

pub struct Tablet {
    level: Level,
    base: Base,
    players: Vec<Player>,

    active_round: Option<Round>,
    rounds: Vec<Round>,
    currently_viewed_round_number: usize,

    /**
     * Virtual -> {@link players}
     */
    indexes: Vec<Option<usize>>,

    /**
     * Virtual
     */
    unbased_indexes: Vec<usize>,

    /**
     * Virtual. Opposite of {@link unbased_indexes}
     */
    based_indexes: Vec<usize>,

    mouse_moved_since_mouse_down: bool,
    last_cursor_position: Vector2f,

    pressed_player_index: Option<usize>,
    drag_target: Option<Rc<DragTarget>>,
    selected_player: Option<usize>,

    popup: RefCell<Option<Popup>>,
    command_line: Textbox,
    show_command: ShowCommand,

    command_manager: CommandManager,
    command_shortcuts: BTreeMap<usize, Vec<String>>,
}

impl Tablet {
    fn make_popup(&self) {
        if self.popup.borrow().is_none() {
            *self.popup.borrow_mut() = Some(Popup::new());
        }
    }

    fn print<T: AsRef<str>>(&self, text: T) {
        self.make_popup();

        self.popup
            .borrow_mut()
            .as_mut()
            .unwrap()
            .print(text.as_ref());
    }

    fn println<T: AsRef<str>>(&self, text: T) {
        self.make_popup();

        self.popup
            .borrow_mut()
            .as_mut()
            .unwrap()
            .println(text.as_ref());
    }

    pub fn player_count(&self) -> usize {
        return self.players.len();
    }

    pub fn get_next_free_index(&self) -> usize {
        let mut i: usize = 0;

        for index in &self.indexes {
            if index.is_none() {
                return i;
            }

            i += 1;
        }

        return i;
    }

    fn get_player(&self, index: usize) -> &Player {
        return &self.players[self.indexes[index].unwrap()];
    }

    fn get_player_mut(&mut self, index: usize) -> &mut Player {
        return &mut self.players[self.indexes[index].unwrap()];
    }

    fn find_player_by_color(&self, color: Color) -> Option<usize> {
        return match self.players.iter().find(|player| player.color() == color) {
            Some(player) => Some(player.index()),
            _ => None,
        };
    }

    fn find_player_by_name(&self, name: &str) -> Option<usize> {
        return match self
            .players
            .iter()
            .find(|player| player.name().to_lowercase() == name)
        {
            Some(player) => Some(player.index()),
            _ => None,
        };
    }

    fn find_player(&self, description: &str) -> Option<usize> {
        if let Some(color) = Color::from_name(description) {
            if let Some(index) = self.find_player_by_color(color) {
                return Some(index);
            }
        }

        if let Some(index) = self.find_player_by_name(description) {
            return Some(index);
        }

        return None;
    }

    fn find_player_at(&self, position: &Vector2f) -> Option<usize> {
        for player in &self.players {
            if player.hitbox().contains(position) {
                return Some(player.index());
            }
        }

        return None;
    }

    fn find_player_under_cursor(&self) -> Option<usize> {
        self.find_player_at(&self.level.screen_to_world(&self.last_cursor_position))
    }

    fn get_selected_player(&self) -> Option<usize> {
        return self.selected_player;
    }

    fn select_player(&mut self, player_index: usize) {
        self.selected_player = Some(player_index);
    }

    fn deselect_player(&mut self) {
        self.selected_player = None;
    }

    fn start_drag(&mut self, player_index: usize, world_position: &Vector2f) {
        if self.based_indexes.contains(&player_index) {
            self.activate_player(player_index);
        }

        let player = self.get_player_mut(player_index);

        self.drag_target = Some(Rc::new(DragTarget {
            player_index,
            offset: *world_position - *player.position(),
        }));
    }

    fn drag(&mut self, world_position: &Vector2f) {
        let drag_target = self.drag_target.as_ref().unwrap().clone();

        let new_position: Vector2f = *world_position - drag_target.offset;

        if new_position.is_in_rectangle(&Vector2f::default(), self.level.get_size()) {
            let player = self.get_player_mut(drag_target.player_index);
            player.set_position(&new_position);
        }
    }

    fn stop_drag(&mut self) {
        self.drag_target = None;
    }

    fn on_mouse_click(&mut self) -> bool {
        if let Some(player_index) = self.find_player_under_cursor() {
            self.select_player(player_index);
            return true;
        }

        return false;
    }

    fn on_mouse_down(&mut self, position: &Vector2f) -> bool {
        self.mouse_moved_since_mouse_down = false;
        let world_position = self.level.screen_to_world(position);

        match self.find_player_at(&world_position) {
            Some(index) => {
                self.pressed_player_index = Some(index);
                return true;
            }
            None => {
                self.deselect_player();
                return false;
            }
        }
    }

    fn on_mouse_move(&mut self, position: &Vector2f, _relative: &Vector2f) -> bool {
        let world_position = self.level.screen_to_world(position);

        if let Some(player_index) = self.pressed_player_index {
            self.start_drag(player_index, &world_position);
            self.pressed_player_index = None;
        }

        self.mouse_moved_since_mouse_down = true;
        self.last_cursor_position = *position;

        if self.drag_target.is_some() {
            self.drag(&world_position);

            return true;
        }

        return false;
    }

    fn on_mouse_up(&mut self) -> bool {
        self.pressed_player_index = None;
        let mut handled = false;

        if self.drag_target.is_some() {
            self.stop_drag();
            handled = true;
        }

        if !self.mouse_moved_since_mouse_down {
            return self.on_mouse_click();
        }

        return handled;
    }

    fn process_text_args(&mut self, command_name: &str, args: &Vec<&str>) {
        if let Some(command) = self.command_manager.get_command_by_name(command_name) {
            if command.can_be_called_with_arguments(self, args) {
                command.call(self, &args.clone());
            } else {
                self.println(format!("usage: {}", command.usage));
            }
        } else {
            self.println(format!("unknown command: {}", command_name));
        }
    }

    fn process_text_command(&mut self, user_input: &str) {
        let words = user_input.split_ascii_whitespace().collect::<Vec<&str>>();

        if !words.is_empty() {
            self.process_text_args(words[0], &words[1..].to_vec());
        }
    }

    fn on_text_command<T: AsRef<str>>(&mut self, user_input: T) {
        self.process_text_command(user_input.as_ref());
        self.deselect_player();
    }

    fn on_character(&mut self, character: char) -> bool {
        if self.command_line.is_visible() {
            self.command_line.on_character(character);
            return true;
        } else {
            if character == ':' {
                self.command_line.show();
            } else {
                self.command_manager.on_character(character);

                if let Some(shortcut) = self.command_manager.get_pending_shortcut() {
                    (shortcut.callback)(self);
                    self.command_manager.cancel_shortcut();
                }

                self.show_command.update(&self.command_manager);
            }

            return true;
        }
    }

    fn on_command_line_key_down(&mut self, keycode: Keycode) -> bool {
        match keycode {
            Keycode::Left => {
                self.command_line.on_arrow(true);
                return true;
            }
            Keycode::Right => {
                self.command_line.on_arrow(false);
                return true;
            }
            Keycode::Backspace => {
                self.command_line.on_backspace();
                return true;
            }
            Keycode::Delete => {
                self.command_line.on_delete();
                return true;
            }
            Keycode::Return => {
                self.on_text_command(self.command_line.get_text().clone());
                self.command_line.set_text("");
                self.command_line.hide();
                return true;
            }
            Keycode::Escape => {
                self.command_line.set_text("");
                self.command_line.hide();
                return true;
            }
            _ => {
                return false;
            }
        }
    }

    fn on_key_down(&mut self, keycode: Keycode) -> bool {
        *self.popup.borrow_mut() = None;

        if self.command_line.is_visible() {
            if self.on_command_line_key_down(keycode) {
                return true;
            }
        } else {
            match keycode {
                Keycode::Escape => {
                    self.command_manager.cancel_shortcut();
                    self.show_command.update(&self.command_manager);
                }
                _ => {}
            }
        }

        return false;
    }

    pub fn handle_event(&mut self, event: &Event) -> bool {
        match event {
            Event::MouseButtonDown { x, y, .. } => {
                return self.on_mouse_down(&Vector2f::new(*x as f32, *y as f32));
            }
            Event::MouseButtonUp { .. } => {
                return self.on_mouse_up();
            }
            Event::MouseMotion {
                x, y, xrel, yrel, ..
            } => {
                return self.on_mouse_move(
                    &Vector2f::new(*x as f32, *y as f32),
                    &Vector2f::new(*xrel as f32, *yrel as f32),
                );
            }
            Event::TextInput { text, .. } => {
                let character = text.chars().next().unwrap();
                return self.on_character(character);
            }
            Event::KeyDown { keycode, .. } => {
                if keycode.is_some() {
                    return self.on_key_down(keycode.unwrap());
                } else {
                    return false;
                }
            }
            _ => {
                return false;
            }
        }
    }

    fn scale_players_to_screen(&mut self) {
        for player in &mut self.players {
            player.set_render_size(
                player.visual_size().x * (self.level.get_render_size().x / self.level.get_size().x),
            );
        }
    }

    fn place_players_on_screen(&mut self) {
        self.scale_players_to_screen();

        for index in self.based_indexes.clone() {
            self.move_player_to_base(index);
        }

        for player in &mut self.players {
            let world_position = player.position();
            let screen_position = self.level.world_to_screen(world_position);
            player.set_render_position(screen_position - player.get_render_size() / 2.0);
        }
    }

    pub fn fit_to_rectangle(&mut self, renderer: &mut Renderer, target: sdl2::rect::Rect) {
        let position = Vector2f::new(target.x() as f32, target.y() as f32);
        let size = Vector2f::new(target.width() as f32, target.height() as f32);

        if self.popup.borrow().is_some() {
            self.popup
                .borrow_mut()
                .as_mut()
                .unwrap()
                .center_in_rectangle(renderer, &position, &size);
        }

        let min_base_width = if self.players.is_empty() {
            64.0
        } else {
            (self.players[0].get_render_size().x * 1.5) * (self.base.get_players_per_row() as f32)
        };

        self.level
            .fit_to_rectangle(renderer, &(size - Vector2f::new(min_base_width, 0.0)));

        self.place_players_on_screen();

        let level_width = self.level.get_render_size().x;

        self.base
            .set_render_position(&Vector2f::new(level_width, 0.0));
        self.base.set_render_size(&Vector2f::new(
            (size.x - level_width).max(min_base_width),
            size.y,
        ));

        let command_line_padding = 4.0;
        let command_line_height = 32.0;

        self.command_line.set_render_position(&Vector2f::new(
            command_line_padding,
            size.y - command_line_height,
        ));
        self.command_line.set_render_size(&Vector2f::new(
            size.x - 2.0 * command_line_padding,
            command_line_height,
        ));

        self.show_command.fit_to_rectangle(&position, &size);
    }

    pub fn pre_render(&mut self, renderer: &mut Renderer) {
        // TODO: Wasteful (@register is no-op after first call)
        for size in &[8, 16, 24, 32, 64] {
            renderer.font_store.register(
                &format!("amongus.{}", size),
                "In your face, joffrey!",
                *size,
            );
        }

        self.fit_to_rectangle(renderer, renderer.canvas.viewport());
    }

    fn add_player(&mut self, player: Player) {
        let virtual_index = player.index();

        if virtual_index >= self.indexes.len() {
            self.indexes.push(Some(self.players.len()));
        }

        self.indexes[virtual_index] = Some(self.players.len());
        self.players.push(player);

        self.base_player(virtual_index);
    }

    fn remove_player(&mut self, player_index: usize) {
        if self.selected_player.is_some() && (self.selected_player.unwrap() == player_index) {
            self.deselect_player();
        }

        if let Some(i) = self.based_indexes.iter().position(|i| *i == player_index) {
            self.based_indexes.remove(i);
        }

        if let Some(i) = self.unbased_indexes.iter().position(|i| *i == player_index) {
            self.unbased_indexes.remove(i);
        }

        let real_index = self.indexes[player_index].unwrap();

        self.players.remove(real_index);

        for i in real_index..self.players.len() {
            if let Some(map) = &mut self.indexes[self.players[i].index()] {
                *map -= 1;
            }
        }

        self.indexes[player_index] = None;
    }

    fn remove_all_players(&mut self) {
        let mut indexes = Vec::<usize>::default();

        for player in &self.players {
            indexes.push(player.index());
        }

        for i in indexes {
            self.remove_player(i);
        }
    }

    fn activate_player(&mut self, player_index: usize) {
        self.unbased_indexes.push(player_index);

        if let Some(i) = self.based_indexes.iter().position(|i| *i == player_index) {
            self.based_indexes.remove(i);
        }
    }

    fn move_player_to_base(&mut self, player_index: usize) {
        let slot = Base::get_player_slot(self.get_player(player_index));
        let screen_position = *self.base.get_render_position() + self.base.get_slot_position(slot);
        let world_position = self.level.screen_to_world(&screen_position);

        let player = &mut self.get_player_mut(player_index);
        player.set_position(&world_position);
    }

    fn base_player(&mut self, player_index: usize) {
        self.based_indexes.push(player_index);
        if let Some(active_index) = self.unbased_indexes.iter().position(|i| *i == player_index) {
            self.unbased_indexes.remove(active_index);
        }

        self.move_player_to_base(player_index);
    }

    fn dump_help(&self) {
        let mut command_handle = 0;

        for command in &self.command_manager.commands {
            let mut is_first_alias = true;

            for alias in &command.aliases {
                if is_first_alias {
                    is_first_alias = false;
                } else {
                    self.print(",");
                }

                self.print(alias);
            }

            self.print(format!(" - {}", command.help).as_str());

            if let Some(shortcut) = self.command_shortcuts.get(&command_handle) {
                self.print(format!(" [{}]", shortcut[0]).as_str());
            }

            self.println("");

            command_handle += 1;
        }
    }

    fn dump_command_help(&self, command: &Command) {
        self.println(&command.help);
        self.println(format!("usage: {}", command.usage));

        let handle = self.command_manager.get_command_handle(command);

        if let Some(shortcut) = self.command_shortcuts.get(&handle) {
            self.println(format!("shortcut: {}", shortcut[0]));
        }
    }

    fn serialize_current_round(&self) -> Round {
        if self.active_round.is_some() {
            return self.active_round.clone().unwrap();
        }

        Round {
            players: self
                .players
                .iter()
                .map(|player| player.serialize())
                .collect(),
            based_colors: self
                .based_indexes
                .iter()
                .map(|i| self.get_player(*i).color())
                .collect(),
        }
    }

    fn save_current_round(&mut self, target_index: usize) {
        assert!(target_index <= self.rounds.len());

        if target_index == self.rounds.len() {
            self.rounds.push(self.serialize_current_round());
        } else {
            self.rounds[target_index] = self.serialize_current_round();
        }
    }

    fn load_round(&mut self, round: &Round) {
        self.remove_all_players();

        for serialized_player in &round.players {
            let mut player = Player::deserialize(&serialized_player);
            let player_index = self.get_next_free_index();

            player.set_index(player_index);
            let color = player.color();
            let position = *player.position();

            self.add_player(player);

            if !round.based_colors.contains(&color) {
                self.activate_player(player_index);
                self.get_player_mut(player_index).set_position(&position);
            }
        }
    }

    fn resume_active_round(&mut self) {
        self.load_round(&self.active_round.clone().unwrap());
        self.currently_viewed_round_number = self.rounds.len() + 1;
        self.active_round = None;
    }

    fn view_past_round(&mut self, round_index: usize) {
        if self.active_round.is_none() {
            self.active_round = Some(self.serialize_current_round());
        }

        self.load_round(&self.rounds[round_index].clone());
        self.currently_viewed_round_number = round_index + 1;
    }

    fn next_round(&mut self) {
        self.save_current_round(self.rounds.len());

        self.base_all_players();

        self.currently_viewed_round_number = self.rounds.len() + 1;
        self.active_round = None;
    }

    fn cmd_help(&mut self, args: &Vec<&str>) {
        if args.is_empty() {
            self.dump_help();
        } else {
            let command_name = args[0];
            if let Some(command) = self.command_manager.get_command_by_name(command_name) {
                self.dump_command_help(&command);
            } else {
                self.dump_help();
            }
        }
    }

    fn cmd_add(&mut self, args: &Vec<&str>) {
        assert!((args.len() >= 1) && (args.len() <= 2));

        if let Some(color) = Color::from_name(args[0]) {
            let index = self.get_next_free_index();

            self.add_player(Player::new(
                index,
                color,
                if args.len() > 1 {
                    args[1].to_string()
                } else {
                    Color::to_string(color).to_string()
                },
            ));
        } else {
            self.println("invalid color");
        }
    }

    fn cmd_color(&mut self, args: &Vec<&str>) {
        assert!(args.len() == 2);

        if let Some(player_index) = self.find_player(args[0]) {
            if let Some(color) = Color::from_name(args[1]) {
                if let Some(player_who_has_the_color_now) = self.find_player_by_color(color) {
                    let old_color = self.get_player(player_index).color();
                    self.get_player_mut(player_who_has_the_color_now)
                        .set_color(old_color);
                }

                self.get_player_mut(player_index).set_color(color);
            } else {
                self.println(format!("no such color {}", args[1]));
            }
        }
    }

    fn cmd_set_alive(&mut self, description: &str, alive: bool) {
        if let Some(player_index) = self.find_player(description) {
            self.get_player_mut(player_index).set_alive(alive);
        } else {
            self.println("no such player");
        }
    }

    fn cmd_kill(&mut self, args: &Vec<&str>) {
        assert!(args.len() == 1);

        self.cmd_set_alive(args[0], false);
    }

    fn cmd_new(&mut self, _args: &Vec<&str>) {
        self.active_round = None;
        self.currently_viewed_round_number = 1;
        self.rounds.clear();

        self.base_all_players();
        self.revive_all_players();
    }

    fn cmd_revive(&mut self, args: &Vec<&str>) {
        if args.is_empty() {
            self.revive_all_players();
        } else if args.len() == 1 {
            self.cmd_set_alive(args[0], true);
        }
    }

    fn cmd_round(&mut self, args: &Vec<&str>) {
        assert!(args.len() == 1);

        if args[0].chars().next().unwrap() == 'n' {
            self.next_round();
        } else if let Ok(round_id) = args[0].parse::<usize>() {
            if round_id == 0 {
                if self.active_round.is_some() {
                    self.resume_active_round();
                }
            } else {
                let round_index = round_id - 1;
                if round_index < self.rounds.len() {
                    self.view_past_round(round_index);
                } else {
                    self.println("no such round");
                }
            }
        }
    }

    fn cmd_delete(&mut self, args: &Vec<&str>) {
        if args.is_empty() {
            self.players.clear();
            self.indexes.clear();
            self.unbased_indexes.clear();
            self.based_indexes.clear();
        } else if args.len() == 1 {
            if let Some(player_index) = self.find_player(args[0]) {
                self.remove_player(player_index);
            }
        }
    }

    fn cmd_trust(&mut self, args: &Vec<&str>) {
        if args.is_empty() {
            for player in &mut self.players {
                player.set_trust(None);
            }
        } else {
            if let Some(player_index) = self.find_player(args[0]) {
                if args.len() == 2 {
                    if let Ok(trust) = Trust::try_from(args[1]) {
                        self.get_player_mut(player_index).set_trust(Some(trust));
                    } else {
                        self.println(format!("no such trust {}", args[1]));
                    }
                } else {
                    self.get_player_mut(player_index).set_trust(None);
                }
            }
        }
    }

    fn cmd_map(&mut self, args: &Vec<&str>) {
        assert!(args.len() == 1);

        if let Some(level) = Level::from_name(args[0]) {
            self.level = level;
        } else {
            self.println(format!("no such map {}", args[0]));
        }
    }

    fn cmd_name(&mut self, args: &Vec<&str>) {
        assert!(args.len() == 2);

        if let Some(player_index) = self.find_player(args[0]) {
            self.get_player_mut(player_index).set_name(args[1]);
        } else {
            self.println(format!("no such player {}", args[0]));
        }
    }

    fn base_all_players(&mut self) {
        for player_index in self.unbased_indexes.clone() {
            self.base_player(player_index);
        }
    }

    fn revive_all_players(&mut self) {
        for player in &mut self.players {
            player.set_alive(true);
        }
    }

    fn cmd_base(&mut self, args: &Vec<&str>) {
        if args.is_empty() {
            self.base_all_players();
        } else {
            if let Some(player_index) = self.find_player(args[0]) {
                self.base_player(player_index);
            }
        }
    }

    fn replace_wildcards(&self, string: &str) -> Option<String> {
        let mut result = string.to_string();

        let wildcard_selected_player = "<selected_player>";

        if result.contains(wildcard_selected_player) {
            if let Some(player_index) = self.get_selected_player() {
                result = result.replace(
                    wildcard_selected_player,
                    self.get_player(player_index).color().to_string().as_str(),
                );
            } else {
                return None;
            }
        }

        return Some(result);
    }

    fn make_text_shortcut_callback(command: &str) -> Rc<dyn Fn(&mut Tablet)> {
        let str_command = command.to_string();

        Rc::new(move |tablet: &mut Tablet| {
            if let Some(processed) = tablet.replace_wildcards(str_command.as_str()) {
                if processed.ends_with(' ') {
                    tablet.command_line.set_text(processed);
                    tablet.command_line.show();
                } else {
                    tablet.on_text_command(processed);
                }
            } else {
                println!("wildcard processing failed for {}", str_command);
            }
        })
    }

    fn register_text_shortcut(&mut self, command_handle: usize, path: &[&str], text: &str) {
        let command_name = self
            .command_manager
            .get_command(command_handle)
            .get_name()
            .clone();

        let full_text = if text.is_empty() {
            command_name
        } else {
            command_name + " " + text
        };

        self.command_manager.register_shortcut(
            path,
            full_text.as_str(),
            Tablet::make_text_shortcut_callback(full_text.as_str()),
        );

        self.command_shortcuts
            .entry(command_handle)
            .or_insert(vec![])
            .push(path[0].to_string());
    }

    fn create_commands(&mut self) {
        self.command_manager.register(
            Command::new(&["help", "?"], Rc::new(Tablet::cmd_help))
                .parameters(CommandParameters::MinMax(0, 1))
                .help("Display help"),
        );

        self.command_manager.register(
            &Command::new(&["insert", "add"], Rc::new(Tablet::cmd_add))
                .parameters(CommandParameters::MinMax(1, 2))
                .usage("<color> [name]")
                .help("Add a player"),
        );

        let color = self.command_manager.register(
            &Command::new(&["color", "c"], Rc::new(Tablet::cmd_color))
                .parameters(CommandParameters::Exactly(2))
                .usage("<description> <color>")
                .help("Change a player's color"),
        );

        self.register_text_shortcut(color, &["c", "c"], "<selected_player> ");

        let name = self.command_manager.register(
            &Command::new(&["name", "n"], Rc::new(Tablet::cmd_name))
                .parameters(CommandParameters::Exactly(2))
                .usage("<description> <name>")
                .help("Change a player's name"),
        );

        self.register_text_shortcut(name, &["n", "c"], "<selected_player> ");

        let kill = self.command_manager.register(
            &Command::new(&["kill", "k"], Rc::new(Tablet::cmd_kill))
                .parameters(CommandParameters::Exactly(1))
                .usage("<description>")
                .help("Kill a player"),
        );

        self.register_text_shortcut(kill, &["k", "c"], "<selected_player>");

        self.command_manager.register(
            &Command::new(&["new"], Rc::new(Tablet::cmd_new))
                .parameters(CommandParameters::Exactly(0))
                .help("Start a new game with the same players"),
        );

        let revive = self.command_manager.register(
            &Command::new(&["revive", "r"], Rc::new(Tablet::cmd_revive))
                .parameters(CommandParameters::MinMax(0, 1))
                .usage("<description>")
                .help("Revive a player"),
        );

        self.register_text_shortcut(revive, &["r", "c"], "<selected_player>");
        self.register_text_shortcut(revive, &["r", "r"], "");

        let delete = self.command_manager.register(
            &Command::new(&["delete", "d"], Rc::new(Tablet::cmd_delete))
                .parameters(CommandParameters::Exactly(1))
                .usage("<description>")
                .help("Delete a player"),
        );

        self.register_text_shortcut(delete, &["d", "d"], "<selected_player>");

        let round = self.command_manager.register(
            &Command::new(&["round", "buffer", "b"], Rc::new(Tablet::cmd_round))
                .parameters(CommandParameters::Exactly(1))
                .usage("<number>")
                .help("Load a round or advance to the next"),
        );

        for i in 0..=9 {
            self.register_text_shortcut(
                round,
                &["b", i.to_string().as_str()],
                i.to_string().as_str(),
            );
        }

        self.register_text_shortcut(round, &["b", "n"], "next");

        let trust = self.command_manager.register(
            &Command::new(&["trust", "t"], Rc::new(Tablet::cmd_trust))
                .parameters(CommandParameters::MinMax(0, 2))
                .usage("[description] [trust]")
                .help("Trust a player. Valid trust values are \"trusted\", \"questionable\", \"suspicious\""),
        );

        self.register_text_shortcut(trust, &["t", "c"], "<selected_player>");
        self.register_text_shortcut(trust, &["t", "t"], "<selected_player> trusted");
        self.register_text_shortcut(trust, &["t", "q"], "<selected_player> questionable");
        self.register_text_shortcut(trust, &["t", "s"], "<selected_player> suspicious");
        self.register_text_shortcut(trust, &["t", "r"], "");

        self.command_manager.register(
            &Command::new(&["map", "m"], Rc::new(Tablet::cmd_map))
                .parameters(CommandParameters::Exactly(1))
                .usage("<map>")
                .help("Change the active map"),
        );

        let base = self.command_manager.register(
            &Command::new(&["base"], Rc::new(Tablet::cmd_base))
                .parameters(CommandParameters::MinMax(0, 1))
                .usage("[description]")
                .help("Base one or all players"),
        );

        self.register_text_shortcut(base, &["g", "g"], "");
        self.register_text_shortcut(base, &["g", "c"], "<selected_player>");
    }

    pub fn new() -> Self {
        let mut s = Self {
            level: Level::new("skeld"),
            base: Base::default(),
            players: Vec::default(),

            active_round: Default::default(),
            rounds: Default::default(),
            currently_viewed_round_number: 1,

            indexes: Vec::default(),

            unbased_indexes: Vec::default(),
            based_indexes: Vec::default(),

            mouse_moved_since_mouse_down: false,
            last_cursor_position: Vector2f::default(),

            pressed_player_index: None,
            drag_target: None,
            selected_player: None,

            popup: RefCell::new(None),
            command_line: Textbox::default(),
            show_command: ShowCommand::new(),

            command_manager: Default::default(),
            command_shortcuts: Default::default(),
        };

        s.create_commands();

        return s;
    }
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct TabletSerialize {
    players: Vec<PlayerSerialize>,
    level: LevelSerialize,
}

impl Serializable<TabletSerialize> for Tablet {
    fn serialize(&self) -> TabletSerialize {
        TabletSerialize {
            players: self
                .players
                .iter()
                .map(|player| player.serialize())
                .collect(),
            level: self.level.serialize(),
        }
    }

    fn deserialize(serialized: &TabletSerialize) -> Self {
        let mut tablet = Tablet::new();

        for serialized_player in &serialized.players {
            let mut player = Player::deserialize(serialized_player);
            player.set_index(tablet.get_next_free_index());
            tablet.add_player(player);
        }

        tablet.level = Level::deserialize(&serialized.level);

        return tablet;
    }
}

impl Tablet {
    fn draw_player_highlight(&self, renderer: &mut Renderer, player_index: usize) {
        const ANIMATION_MIN: f32 = 0.5;
        let animation =
            ANIMATION_MIN + renderer.animation().scale(1.5).sin() / (1.0 / ANIMATION_MIN);

        let player_size = self.get_player(player_index).get_render_size();
        let static_highlight_size = Vector2f::new(player_size.x, player_size.y / 2.0);
        let highlight_size = static_highlight_size * animation;
        let half_highlight_size = highlight_size / 2.0;

        let player_position = *self.get_player(player_index).get_render_position();
        let player_feet = player_position + Vector2f::new(player_size.x / 2.0, player_size.y);
        let highlight_top =
            player_feet + Vector2f::new(0.0, (static_highlight_size.y - highlight_size.y) / 2.0);

        let vertexes = vec![
            highlight_top,
            Vector2f::new(
                highlight_top.x + half_highlight_size.x,
                highlight_top.y + highlight_size.y,
            ),
            Vector2f::new(
                highlight_top.x - half_highlight_size.x,
                highlight_top.y + highlight_size.y,
            ),
        ];

        shape::Polygon::new(&vertexes, sdl2::pixels::Color::WHITE)
            .filled()
            .outlined()
            .outline_color(self.get_player(player_index).color().to_sdl2())
            .draw(renderer);
    }

    fn draw_players(&self, renderer: &mut Renderer) {
        if let Some(player_index) = self.get_selected_player() {
            self.draw_player_highlight(renderer, player_index);
        }

        for player in &self.players {
            player.draw(renderer);
        }
    }

    fn draw_round_indicator(&self, renderer: &mut Renderer) {
        let round_count = self.rounds.len() + 1;
        let round_size = Vector2f::new(32.0, 32.0);
        let half_round_size = round_size / 2.0;
        let total_size = Vector2f::new(round_size.x * round_count as f32, round_size.y);

        let position = Vector2f::new(0.0, 0.0);

        shape::Rectangle::new(&position, &total_size, sdl2::pixels::Color::BLACK)
            .filled()
            .draw(renderer);

        for i in 0..round_count {
            let round_number = i + 1;
            let round_label = if round_number == round_count {
                0
            } else {
                round_number
            }
            .to_string();

            let round_position = Vector2f::new(round_size.x * (i as f32), position.y);

            shape::Text::new(
                round_label.as_str(),
                "amongus.24",
                &(round_position + half_round_size),
                sdl2::pixels::Color::WHITE,
            )
            .center(true, true)
            .draw(renderer);
        }

        let currently_viewed_round_position = Vector2f::new(
            round_size.x * ((self.currently_viewed_round_number - 1) as f32),
            position.y,
        );

        shape::Rectangle::new(
            &currently_viewed_round_position,
            &round_size,
            sdl2::pixels::Color::WHITE,
        )
        .draw(renderer);
    }
}
impl Drawable for Tablet {
    fn draw(&self, renderer: &mut Renderer) {
        self.level.draw(renderer);

        self.base.draw(renderer);

        self.draw_players(renderer);

        self.command_line.draw(renderer);

        self.show_command.draw(renderer);

        self.draw_round_indicator(renderer);

        if self.popup.borrow().is_some() {
            self.popup.borrow().as_ref().unwrap().draw(renderer);
        }
    }
}
