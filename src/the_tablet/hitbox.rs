use super::vector::Vector2f;

pub trait Hitbox {
    fn contains(&self, position: &Vector2f) -> bool;
}

pub struct HitRectangle {
    pub top_left: Vector2f,
    pub bottom_right: Vector2f,
}

impl Hitbox for HitRectangle {
    fn contains(&self, position: &Vector2f) -> bool {
        return (position.x >= self.top_left.x)
            && (position.x <= self.bottom_right.x)
            && (position.y >= self.top_left.y)
            && (position.y <= self.bottom_right.y);
    }
}
