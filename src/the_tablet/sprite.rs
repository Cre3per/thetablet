use sdl2::rect::Rect;
use sdl2::render::Texture;

use super::{
    renderer::{drawable::Drawable, renderer::Renderer},
    vector::Vector2f,
};

use std::rc::Rc;

pub struct Sprite<'s> {
    position: Vector2f,
    size: Vector2f,

    original_size: Vector2f,

    texture: Rc<Texture<'s>>,
}

impl<'s> Sprite<'s> {
    fn get_texture_size(texture: &Texture) -> Vector2f {
        let query = texture.query();

        return Vector2f::new(query.width as f32, query.height as f32);
    }

    pub fn from_texture(texture: Rc<Texture<'s>>) -> Self {
        let texture_size = Sprite::get_texture_size(&texture);

        Self {
            position: Vector2f::default(),
            size: texture_size,
            original_size: texture_size,
            texture,
        }
    }

    pub fn from_resource(renderer: &mut Renderer<'s>, key: &str) -> Self {
        let texture = renderer.texture_store.get(&key.to_string());

        return Sprite::from_texture(texture);
    }

    pub fn set_position(&mut self, position: &Vector2f) {
        self.position = *position;
    }

    pub fn get_position(&self) -> &Vector2f {
        &self.position
    }

    pub fn set_size(&mut self, size: &Vector2f) {
        self.size = *size;
    }

    pub fn get_size(&self) -> &Vector2f {
        &self.size
    }

    pub fn get_original_size(&self) -> &Vector2f {
        return &self.original_size;
    }

    pub fn fit_to_rectangle(&mut self, rectangle: &Vector2f) {
        let texture_rect = *self.get_original_size();
        let ratios = texture_rect / *rectangle;

        let max_ratio = if ratios.x > ratios.y {
            ratios.x
        } else {
            ratios.y
        };

        self.set_size(&(texture_rect / max_ratio));
    }
}

impl<'s> Drawable for Sprite<'s> {
    fn draw(&self, renderer: &mut Renderer) {
        renderer
            .canvas
            .copy(
                &self.texture,
                None,
                Rect::new(
                    self.position.x as i32,
                    self.position.y as i32,
                    self.size.x as u32,
                    self.size.y as u32,
                ),
            )
            .unwrap();
    }
}
