use std::rc::Rc;

use super::{
    command::Command,
    tree::{Tree, TreeElement},
    Tablet,
};

pub type ShortcutCallback = dyn Fn(&mut Tablet);

#[derive(Clone)]
pub struct Shortcut {
    pub help: String,
    pub callback: Rc<ShortcutCallback>,
}

#[derive(Default)]
pub struct CommandManager {
    pub commands: Vec<Rc<Command>>,

    shortcuts: Tree<String, Rc<Shortcut>>,
    active_shortcut_path: Vec<String>,
}

impl CommandManager {
    pub fn get_command_by_name(&self, alias: &str) -> Option<Rc<Command>> {
        if let Some(found) = self
            .commands
            .iter()
            .find(|command| command.is_named_by(alias))
        {
            return Some(found.clone());
        }

        return None;
    }

    pub fn get_command_handle(&self, command: &Command) -> usize {
        self.commands
            .iter()
            .position(|el| el.aliases == command.aliases)
            .unwrap()
    }

    fn is_known_alias(&self, aliases: &Vec<String>) -> Option<String> {
        match aliases
            .iter()
            .find(|alias| self.get_command_by_name(alias).is_some())
        {
            Some(alias) => Some(alias.clone()),
            None => None,
        }
    }

    pub fn get_command(&self, command_handle: usize) -> Rc<Command> {
        assert!(command_handle < self.commands.len());
        return self.commands[command_handle].clone();
    }

    pub fn register(&mut self, command: &Command) -> usize {
        if let Some(alias) = self.is_known_alias(&command.aliases) {
            panic!(
                "cannot register command {}, because a command with the same alias already exists",
                alias
            );
        }

        let id = self.commands.len();

        self.commands.push(Rc::new(command.clone()));

        return id;
    }

    pub fn register_shortcut(&mut self, path: &[&str], help: &str, callback: Rc<ShortcutCallback>) {
        let vpath: Vec<String> = path.iter().map(|x| x.to_string()).collect();
        self.shortcuts.add_value(
            &vpath,
            &Rc::new(Shortcut {
                help: help.to_string(),
                callback,
            }),
        );
    }

    pub fn get_active_shortcut_path(&self) -> &Vec<String> {
        &self.active_shortcut_path
    }

    pub fn cancel_shortcut(&mut self) {
        self.active_shortcut_path.clear();
    }

    pub fn get_pending_shortcut(&self) -> Option<Rc<Shortcut>> {
        self.shortcuts.walk_path(&self.active_shortcut_path)
    }

    pub fn get_shortcut_candidates(&self) -> Vec<TreeElement<String, Rc<Shortcut>>> {
        self.shortcuts.walk_subpath(&self.active_shortcut_path)
    }

    pub fn on_character(&mut self, character: char) -> bool {
        self.active_shortcut_path.push(character.to_string());

        if self
            .shortcuts
            .walk_subpath(&self.active_shortcut_path)
            .is_empty()
        {
            self.cancel_shortcut();
        }

        return true;
    }
}
