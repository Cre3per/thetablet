#[derive(Clone)]
pub struct TreeElement<Key, Value> {
    pub path: Vec<Key>,
    pub value: Value,
}

#[derive(Clone)]
pub struct Tree<Key, Value> {
    // I'm incapable of implementing a real tree
    root: Vec<TreeElement<Key, Value>>,
}

impl<Key, Value> Default for Tree<Key, Value> {
    fn default() -> Self {
        Self {
            root: Default::default(),
        }
    }
}

impl<Key: std::cmp::Eq + Clone, Value: Clone> Tree<Key, Value> {
    pub fn walk_subpath(&self, path: &Vec<Key>) -> Vec<TreeElement<Key, Value>> {
        return self
            .root
            .iter()
            .filter(|element| element.path.starts_with(path.as_slice()))
            .map(|element| element.clone())
            .collect();
    }

    pub fn walk_path(&self, path: &Vec<Key>) -> Option<Value> {
        return match self.root.iter().find(|element| element.path == *path) {
            Some(element) => Some(element.value.clone()),
            None => None,
        };
    }

    pub fn add_value(&mut self, path: &Vec<Key>, value: &Value) {
        assert!(self.walk_subpath(path).is_empty());

        self.root.push(TreeElement {
            path: path.clone(),
            value: value.clone(),
        });
    }
}
