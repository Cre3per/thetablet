use super::{
    renderer::{drawable::Drawable, renderer::Renderer},
    sprite::*,
    vector::Vector2f,
};

struct Part<'s> {
    sprite: Sprite<'s>,
    offset: Vector2f,
    scale: Vector2f,
}

impl<'s> Drawable for Part<'s> {
    fn draw(&self, renderer: &mut Renderer) {
        self.sprite.draw(renderer);
    }
}

pub struct SpriteGroup<'s> {
    parts: Vec<Part<'s>>,
}

impl<'s> SpriteGroup<'s> {
    pub fn get_sprite(&mut self, index: usize) -> &mut Sprite<'s> {
        return &mut self.parts[index].sprite;
    }

    pub fn set_position(&mut self, position: &Vector2f) {
        for part in &mut self.parts {
            part.sprite.set_position(&(*position + part.offset));
        }
    }

    pub fn set_size(&mut self, size: &Vector2f) {
        for part in &mut self.parts {
            let new_size = part.scale * *size;
            part.sprite.set_size(&new_size);
        }
    }

    pub fn set_scale(&mut self, size: f32) {
        for part in &mut self.parts {
            part.sprite
                .set_size(&(*part.sprite.get_original_size() * part.scale * size));
        }
    }

    pub fn new(sprites: Vec<Sprite<'s>>) -> Self {
        let mut parts: Vec<Part> = Vec::new();

        for sprite in sprites {
            let part = Part {
                offset: *sprite.get_position(),
                scale: *sprite.get_size() / *sprite.get_original_size(),
                sprite,
            };

            parts.push(part);
        }

        Self { parts }
    }
}

impl<'s> Drawable for SpriteGroup<'s> {
    fn draw(&self, renderer: &mut Renderer) {
        for part in &self.parts {
            part.draw(renderer);
        }
    }
}
