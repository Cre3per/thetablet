use super::{
    renderer::{drawable::Drawable, renderer::Renderer, shape},
    vector::Vector2f,
};

#[derive(Default, Clone)]
pub struct Popup {
    position: Vector2f,
    text: String,
}

impl Popup {
    pub fn center_in_rectangle(
        &mut self,
        renderer: &mut Renderer,
        position: &Vector2f,
        size: &Vector2f,
    ) {
        let text_size = self.get_text_size(renderer);
        self.position = *position + (*size / 2.0) - (text_size / 2.0);
    }

    pub fn print(&mut self, text: &str) {
        self.text += text;
    }

    pub fn println(&mut self, text: &str) {
        self.text += (text.to_string() + "\n").as_str();
    }

    pub fn new() -> Self {
        Self::default()
    }
}

impl From<&str> for Popup {
    fn from(text: &str) -> Self {
        Self {
            position: Vector2f::default(),
            text: text.to_string(),
        }
    }
}

impl From<&Vec<&str>> for Popup {
    fn from(text: &Vec<&str>) -> Self {
        let mut str_text = String::default();
        let mut is_first = true;

        for line in text {
            if is_first {
                is_first = false;
            } else {
                str_text += "\n";
            }

            str_text += line;
        }

        Self::from(str_text.as_str())
    }
}

impl Popup {
    const fn get_font() -> &'static str {
        return "amongus.24";
    }

    const fn get_line_height() -> f32 {
        32.0
    }

    fn get_lines(&self) -> Vec<String> {
        let mut lines: Vec<String> = self.text.split('\n').map(|e| e.to_string()).collect();

        if let Some(last_line) = lines.last() {
            if last_line.is_empty() {
                lines.remove(lines.len() - 1);
            }
        }

        return lines;
    }

    fn get_text_size(&self, renderer: &mut Renderer) -> Vector2f {
        let lines = self.get_lines();
        let mut size = Vector2f::new(0.0, (lines.len() as f32) * Popup::get_line_height());

        for line in lines {
            size.x = size.x.max(
                shape::Text::new(
                    line.as_str(),
                    Popup::get_font(),
                    &Vector2f::default(),
                    sdl2::pixels::Color::MAGENTA,
                )
                .size(renderer)
                .x,
            );
        }

        return size;
    }
}

impl Drawable for Popup {
    fn draw(&self, renderer: &mut super::renderer::renderer::Renderer) {
        let padding = Vector2f::new(4.0, 4.0);

        let text_size = self.get_text_size(renderer);

        shape::Rectangle::new(
            &(self.position - padding),
            &(text_size + padding * 2.0),
            sdl2::pixels::Color::BLACK,
        )
        .filled()
        .outlined()
        .outline_color(sdl2::pixels::Color::WHITE)
        .draw(renderer);

        let mut i = 0;
        for line in self.get_lines() {
            shape::Text::new(
                line.as_str(),
                Popup::get_font(),
                &(self.position + Vector2f::new(0.0, (i as f32) * Popup::get_line_height())),
                sdl2::pixels::Color::WHITE,
            )
            .draw(renderer);

            i += 1;
        }
    }
}
